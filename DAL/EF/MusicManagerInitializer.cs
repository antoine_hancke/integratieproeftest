﻿using System;
using MusicManager.BL.Domain;

namespace MusicManager.DAL.EF
{
    internal class MusicManagerInitializer
    {
        private static bool _initialized;

        public static void Initialize(MusicManagerDbContext dbContext, bool rebuild)
        {
            if (!_initialized)
            {
                if (rebuild)
                { 
                    dbContext.Database.EnsureDeleted();
                }

                if (dbContext.Database.EnsureCreated())
                {
                    Seed(dbContext);
                }

                _initialized = true;
            }
        }

        static void Seed(MusicManagerDbContext dbContext)
        {
            // Record Labels
            RecordLabel recordLabel1 = new RecordLabel("YEAR0001",
                new DateTime(2015, 1, 1), "Stockholm - Sweden", null);
            RecordLabel recordLabel2 = new RecordLabel("Universal",
                new DateTime(1981, 12, 5), "New York - USA", null);
            RecordLabel recordLabel3 = new RecordLabel("Pirate Gang",
                new DateTime(2021, 10, 1), "Antwerp - Belgium", null);

            // Managers
            Manager manager1 = new Manager("Dirk", "Janssens");
            Manager manager2 = new Manager("Thibault", "Poels");
            Manager manager3 = new Manager("Jim", "Halpert");
            Manager manager4 = new Manager("Michael", "Scott");
            Manager manager5 = new Manager("Rick", "Owens");
            
            
            // Artists
            
            Artist artist1 = new Artist("Benjamin Reichwald", "Bladee", null, 156611, 215353, recordLabel1,manager1);
            Artist artist2 = new Artist("Barry White", "Barry White", null, 2015688, 1364554, recordLabel2,manager2);
            Artist artist3 = new Artist("Randy Goffe", "Home", null, 665001, 212127, recordLabel2,manager3);
            Artist artist4 = new Artist("Jonatan Leandoer", "Yung Lean", null, 890494, 590881, recordLabel1,manager4);
            Artist artist5 = new Artist("Jakub", "Sheg", null, 100, 93, recordLabel3,manager5);
            
            
            // Songs
            
            Song song1 = new Song("Hennesy & Sailer Moon", 14034610,
                new DateTime(2016, 4, 19), new TimeSpan(0, 4, 14), null, Genre.Rnb);
            Song song2 = new Song("Let's Ride", null,
                new DateTime(2021, 5, 30), new TimeSpan(0, 2, 25), null, Genre.Rap);
            Song song3 = new Song("Resonance", 93486077,
                new DateTime(2014, 2, 10), new TimeSpan(0, 3, 32), null, Genre.Edm);
            Song song4 = new Song("You're the first, you're the last, my everything", 16819619,
                new DateTime(1974, 2, 1), new TimeSpan(0, 3, 32), null, Genre.Other);
            Song song5 = new Song("Fairy Tale", 482,
                new DateTime(2021, 4, 1), new TimeSpan(0, 3, 10), null, Genre.Rap);

            // Connections
            
            // Record Labels - Artists
            recordLabel1.Artists.Add(artist1);
            recordLabel1.Artists.Add(artist4);
            recordLabel2.Artists.Add(artist2);
            recordLabel2.Artists.Add(artist3);
            recordLabel3.Artists.Add(artist5);
            
            // Managers - Artists
            manager1.Artist = artist1;
            manager2.Artist = artist2;
            manager3.Artist = artist3;
            manager4.Artist = artist4;
            manager5.Artist = artist5;

            // Artists - (SongContributions) - Songs
            SongContribution songContribution1 = new SongContribution()
            {
                Artist = artist1,
                Contribution = SongContributionType.Vocalist,
                Song = song1
            };
            SongContribution songContribution2 = new SongContribution()
            {
                Artist = artist4,
                Contribution = SongContributionType.Vocalist,
                Song = song1
            };
            SongContribution songContribution3 = new SongContribution()
            {
                Artist = artist1,
                Contribution = SongContributionType.Vocalist,
                Song = song2
            };
            SongContribution songContribution4 = new SongContribution()
            {
                Artist = artist3,
                Contribution = SongContributionType.AudioEngineer,
                Song = song3
            };
            SongContribution songContribution5 = new SongContribution()
            {
                Artist = artist2,
                Contribution = SongContributionType.Vocalist,
                Song = song4
            };
            SongContribution songContribution6 = new SongContribution()
            {
                Artist = artist5,
                Contribution = SongContributionType.BeatMaker,
                Song = song5
            };

            song1.SongContributions.Add(songContribution1);
            song1.SongContributions.Add(songContribution2);
            song2.SongContributions.Add(songContribution3);
            song3.SongContributions.Add(songContribution4);
            song4.SongContributions.Add(songContribution5);
            song5.SongContributions.Add(songContribution6);
            
            // Songs - (SongContributions) - Artists
            artist1.SongContributions.Add(songContribution1);
            artist1.SongContributions.Add(songContribution3);
            artist2.SongContributions.Add(songContribution5);
            artist3.SongContributions.Add(songContribution4);
            artist4.SongContributions.Add(songContribution2);
            artist5.SongContributions.Add(songContribution6);
            
            // Add to DB
            dbContext.Artists.Add(artist1);
            dbContext.Artists.Add(artist2);
            dbContext.Artists.Add(artist3);
            dbContext.Artists.Add(artist4);
            dbContext.Artists.Add(artist5);
            
            dbContext.Songs.Add(song1);
            dbContext.Songs.Add(song2);
            dbContext.Songs.Add(song3);
            dbContext.Songs.Add(song4);
            dbContext.Songs.Add(song5);
            
            dbContext.RecordLabels.Add(recordLabel1);
            dbContext.RecordLabels.Add(recordLabel2);
            dbContext.RecordLabels.Add(recordLabel3);

            dbContext.SongContributions.Add(songContribution1);
            dbContext.SongContributions.Add(songContribution2);
            dbContext.SongContributions.Add(songContribution3);
            dbContext.SongContributions.Add(songContribution4);
            dbContext.SongContributions.Add(songContribution5);
            dbContext.SongContributions.Add(songContribution6);
            
            dbContext.Managers.Add(manager1);
            dbContext.Managers.Add(manager2);
            dbContext.Managers.Add(manager3);
            dbContext.Managers.Add(manager4);
            dbContext.Managers.Add(manager5);
            
            // Save changes - clear Track state
            dbContext.SaveChanges();
            dbContext.ChangeTracker.Clear();
        }
    }
}
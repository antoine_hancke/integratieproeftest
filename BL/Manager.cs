﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MusicManager.DAL;
using MusicManager.BL.Domain;

namespace MusicManager.BL
{
    public class Manager : IManager
    {

        private readonly IRepository _repository;

        public Manager(IRepository repository)
        {
            _repository = repository;
        }
        
        // Artist
        
        public Artist GetArtist(int id)
        {
            return _repository.ReadArtist(id);
        }

        public Artist GetArtistWithRecordLabelAndManager(int id)
        {
            return _repository.ReadArtistWithRecordLabelAndManager(id);
        }

        public IEnumerable<Artist> GetAllArtists()
        {
            return _repository.ReadAllArtists();
        }

        public IEnumerable<Artist> GetAllArtistsWithRecordLabel()
        {
            return _repository.ReadAllArtistsWithRecordLabel();
        }

        public IEnumerable<Artist> GetArtistsByAliasOrByAmountOfFollowers(string alias = null, int? amountOfFollowers = null)
        {
            return _repository.ReadArtistsByAliasOrByAmountOfFollowers(alias, amountOfFollowers);
        }

        public Artist AddArtist(string name, string alias, IList<SongContribution> songContributions, float totalRevenue, int amountOfFollowers,
            RecordLabel signedRecordLabel, Domain.Manager manager)
        {
            Artist artist = new Artist(name, alias, songContributions, totalRevenue, amountOfFollowers, signedRecordLabel, manager);
            this.Validate(artist);
            _repository.CreateArtist(artist);
            return artist;
        }

        private void Validate(Artist artist)
        {
            IList<ValidationResult> errors = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(artist, new ValidationContext(artist)
                , errors, validateAllProperties: true);

            StringBuilder errorMessage = new StringBuilder();
            
            foreach(ValidationResult validationResult in errors)
            {
                errorMessage.Append(validationResult.ErrorMessage + " ");
            }
            
            if (!valid) throw new ValidationException(errorMessage.ToString());
        }
        
        // Song

        public Song GetSong(int id)
        {
            return _repository.ReadSong(id);
        }

        public Song GetSongWithArtists(int id)
        {
            return _repository.ReadSongWithArtists(id);
        }

        public IEnumerable<Song> GetAllSongs()
        {
            return _repository.ReadAllSongs();
        }

        public IEnumerable<Song> GetAllSongsWithArtists()
        {
            return _repository.ReadAllSongsWithArtists();
        }

        public IEnumerable<Song> GetSongsOfArtist(int artistId)
        {
            return _repository.ReadSongsOfArtist(artistId);
        }

        public IEnumerable<Song> GetSongByGenre(Genre genre)
        {
            return _repository.ReadSongByGenre(genre);
        }

        public IEnumerable<Song> GetSongsNotOfArtist(int artistId)
        {
            return _repository.ReadSongsNotOfArtist(artistId);
        }

        public Song AddSong(string title, long? amountOfStreams, DateTime releaseDate, TimeSpan length, IList<SongContribution> songContributions, Genre genre)
        {
            Song song = new Song(title, amountOfStreams, releaseDate, length, songContributions, genre);
            this.Validate(song);
            
            _repository.CreateSong(song);
            return song;
        }
        
        private void Validate(Song song)
        {
            IList<ValidationResult> errors = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(song, new ValidationContext(song)
                , errors, validateAllProperties: true);
            
            StringBuilder errorMessage = new StringBuilder();
            
            foreach(ValidationResult validationResult in errors)
            {
                errorMessage.Append(validationResult.ErrorMessage + " ");
            }
            
            if (!valid) throw new ValidationException(errorMessage.ToString());
        }

        // RecordLabel
        
        public RecordLabel GetRecordLabel(int id)
        {
            return _repository.ReadRecordLabel(id);
        }

        public IEnumerable<RecordLabel> GetAllRecordLabels()
        {
            return _repository.ReadAllRecordLabels();
        }

        public RecordLabel AddRecordLabel(string name, DateTime dateFounded, string location, List<Artist> artists)
        {
            RecordLabel recordLabel = new RecordLabel(name, dateFounded, location, artists);
            this.Validate(recordLabel);
            
            _repository.CreateRecordLabel(recordLabel);
            return recordLabel;
        }

        public void ChangeRecordLabel(int id, RecordLabel updatedRecordLabel)
        {
            _repository.UpdateRecordLabel(id, updatedRecordLabel);
        }

        private void Validate(RecordLabel recordLabel)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(recordLabel, new ValidationContext(recordLabel)
                , errors, validateAllProperties: true);
            
            StringBuilder errorMessage = new StringBuilder();
            
            foreach(ValidationResult validationResult in errors)
            {
                errorMessage.Append(validationResult.ErrorMessage + " ");
            }
            
            if (!valid) throw new ValidationException(errorMessage.ToString());
        }
        
        // Manager

        public Domain.Manager GetManager(int id)
        {
            return _repository.ReadManager(id);
        }

        public IEnumerable<Domain.Manager> GetAllManagers()
        {
            return _repository.ReadAllManagers();
        }

        public Domain.Manager AddManager(string firstName, string lastName)
        {
            Domain.Manager manager = new Domain.Manager(firstName,lastName);
            this.Validate(manager);
            
            _repository.CreateManager(manager);
            return manager;
        }

        public SongContribution GetSongContribution(int songId, int artistId)
        {
            return _repository.ReadSongContribution(songId, artistId);
        }


        private void Validate(Domain.Manager manager)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(manager, new ValidationContext(manager)
                , errors, validateAllProperties: true);
            
            StringBuilder errorMessage = new StringBuilder();
            
            foreach(ValidationResult validationResult in errors)
            {
                errorMessage.Append(validationResult.ErrorMessage + " ");
            }
            
            if (!valid) throw new ValidationException(errorMessage.ToString());
        }
        
        // SongContribution
        
        public SongContribution AddSongContribution(int songId, int artistId,SongContributionType songContributionType = 0)
        {
            SongContribution songContribution = new SongContribution()
            {
                Artist = GetArtist(artistId),
                Song = GetSong(songId),
                Contribution = songContributionType

            };
            this.Validate(songContribution);
            
            _repository.CreateSongContribution(songContribution);
            return songContribution;
        }

        public void RemoveSongContribution(int songId, int artistId)
        {
            _repository.DeleteSongContribution(songId, artistId);
        }
        
        private void Validate(SongContribution songContribution)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(songContribution, new ValidationContext(songContribution)
                , errors, validateAllProperties: true);
            
            StringBuilder errorMessage = new StringBuilder();
            
            foreach(ValidationResult validationResult in errors)
            {
                errorMessage.Append(validationResult.ErrorMessage + " ");
            }
            
            if (!valid) throw new ValidationException(errorMessage.ToString());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using MusicManager.BL.Domain;

namespace MusicManager.BL
{
    public interface IManager
    {
        // Artist
        
        public Artist GetArtist(int id);

        public Artist GetArtistWithRecordLabelAndManager(int id);
        
        public IEnumerable<Artist> GetAllArtists();
        
        public IEnumerable<Artist> GetAllArtistsWithRecordLabel();

        public IEnumerable<Artist> GetArtistsByAliasOrByAmountOfFollowers(string alias = null, int? amountOfFollowers = null);

        public Artist AddArtist(string name, string alias, IList<SongContribution> songContributions, float totalRevenue, int amountOfFollowers, RecordLabel signedRecordLabel, Domain.Manager manager);
        
        // Song
        
        public Song GetSong(int id);

        public Song GetSongWithArtists(int id);
        
        public IEnumerable<Song> GetAllSongs();

        public IEnumerable<Song> GetAllSongsWithArtists();

        public IEnumerable<Song> GetSongsOfArtist(int artistId);

        public IEnumerable<Song> GetSongByGenre(Genre genre);
        
        public IEnumerable<Song> GetSongsNotOfArtist(int artistId);

        public Song AddSong(string title, long? amountOfStreams, DateTime releaseDate, TimeSpan length, IList<SongContribution> songContributions, Genre genre);

        // RecordLabel
        
        public RecordLabel GetRecordLabel(int id);
        
        public IEnumerable<RecordLabel> GetAllRecordLabels();

        public RecordLabel AddRecordLabel(string name, DateTime dateFounded, string location, List<Artist> artists);

        public void ChangeRecordLabel(int id, RecordLabel updatedRecordLabel);
        
        // Manager
        
        public Domain.Manager GetManager(int id);
        
        public IEnumerable<Domain.Manager> GetAllManagers();

        public Domain.Manager AddManager(string firstName, string lastName);
        
        // SongContribution

        public SongContribution GetSongContribution(int songId, int artistId);
        
        public SongContribution AddSongContribution(int songId, int artistId, SongContributionType songContributionType = 0);
        public void RemoveSongContribution(int songId, int artistId);
    }
}
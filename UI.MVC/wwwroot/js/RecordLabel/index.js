﻿const loadRecordLabelsButton =
    document.getElementById("loadRecordLabels");

loadRecordLabelsButton.addEventListener("click", loadRecordLabels);
window.addEventListener('load',loadRecordLabels);


function loadRecordLabels(){
    fetch("/api/RecordLabels",
        {
            method: "GET",
            headers: {
                "Accept":"application/json"
            }
    })
    
        .then(response => response.json())
        .then(data => showRecordLabels(data))
        .catch(reason => alert("Call failed: " + reason));
}

function showRecordLabels(recordLabels) {
    const tableBody = document.getElementById("recordLabelTableBody");
    tableBody.innerHTML = "";
    
    recordLabels.forEach(recordLabel => addRecordLabel(recordLabel));
}

function addRecordLabel(recordLabel) {
    const tableBody = document.getElementById("recordLabelTableBody");
    tableBody.innerHTML += `<tr> <td>${recordLabel.name}</td> <td>${formatDate(recordLabel.dateFounded)}</td> 
    <td><a href="/RecordLabel/Detail/${recordLabel.id}?recordLabelId=${recordLabel.id}">Details</a></td> </tr>`;
}

function formatDate(date) {
    const dateWithoutTime = date.split("T")[0];
    const year = dateWithoutTime.split("-")[0];
    const month = dateWithoutTime.split("-")[1];
    const day = dateWithoutTime.split("-")[2];
    
    return day.concat("/").concat(month).concat("/").concat(year);
}
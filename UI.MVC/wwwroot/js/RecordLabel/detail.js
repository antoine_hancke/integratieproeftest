﻿const urlParams = new URLSearchParams(window.location.search);
const loadRecordLabelId = parseInt(urlParams.get("recordLabelId"));
const saveChangesButton = document.getElementById("save");

saveChangesButton.addEventListener('click',saveRecordLabelDetails)
window.addEventListener('load',loadRecordLabelDetails);

function loadRecordLabelDetails() {
    fetch(`/api/RecordLabels/${loadRecordLabelId}?recordLabelId=${loadRecordLabelId}`,
        {
            method: "GET",
            headers: {
                "Accept":"application/json"
            }
        })

        .then(response => response.json())
        .then(data => showRecordLabelDetail(data))
        .catch(reason => alert("Call failed: "+ reason));
}

function showRecordLabelDetail(recordLabel){
    document.getElementById("name").value = recordLabel.name;
    document.getElementById("dateFounded").value = formatDateForUpdate(recordLabel.dateFounded);
    document.getElementById("location").value = recordLabel.location;
}

function saveRecordLabelDetails(){
    const newName = document.getElementById("name").value;
    const newDateFounded = document.getElementById("dateFounded").value;
    const newLocation = document.getElementById("location").value;
    
    const updatedRecordLabel = {name: newName, dateFounded: newDateFounded, location: newLocation};
    fetch(`/api/RecordLabels/${loadRecordLabelId}?recordLabelId=${loadRecordLabelId}`,
        {
            method: "PUT",
            body: JSON.stringify(updatedRecordLabel),
            headers: {
                "Content-Type":"application/json"
            }
        })
        .then(r => {
            if (r.status === 400) {
                return r.json().then(er => showValidationError(er));
            } else if(r.status === 204){
                return showChangesConfirmation()
            }
        })
        .catch(reason => alert("Call failed: "+ reason));

}

function showValidationError(error){
    const confirmationSpan = document.getElementById("confirmationSpan");
    const nameValidationSpan = document.getElementById("nameValidation");
    const dateFoundedValidation = document.getElementById("dateFoundedValidation");
    confirmationSpan.innerText = "";
    nameValidationSpan.innerText = "";
    dateFoundedValidation.innerText = "";
    
    if(error.Name !== undefined){
        nameValidationSpan.innerText = error.Name.join(" ");
    } else {
        dateFoundedValidation.innerText = "This is not a valid date!";
    }
}

function showChangesConfirmation(){
    const confirmationSpan = document.getElementById("confirmationSpan");
    const nameValidationSpan = document.getElementById("nameValidation");
    const dateFoundedValidation = document.getElementById("dateFoundedValidation");
    nameValidationSpan.innerText = "";
    dateFoundedValidation.innerText = "";
    confirmationSpan.innerText = "Changes saved!"
}

function formatDateForUpdate(date) {
    const dateWithoutTime = date.split("T")[0];
    const year = dateWithoutTime.split("-")[0];
    const month = dateWithoutTime.split("-")[1];
    const day = dateWithoutTime.split("-")[2];

    // For updating, the format of a date has to be the following: yyyy-MM-dd
    // This is required by the form for value change purposes.
    return year.concat("-").concat(month).concat("-").concat(day);
}
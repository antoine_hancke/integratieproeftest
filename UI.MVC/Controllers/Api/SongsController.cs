﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MusicManager.BL;
using MusicManager.BL.Domain;
using UI.MVC.Models;

namespace UI.MVC.Controllers.Api
{
    [Route("api/[controller]")]
    public class SongsController : ControllerBase
    {
        private readonly IManager _manager;

        public SongsController(IManager manager)
        {
            _manager = manager;
        }
        
        // GET
        [HttpGet("ofArtist/{id}")]
        public IActionResult GetSongsOfArtist(int id)
        {
            if (!_manager.GetSongsOfArtist(id).Any())
            {
                return NoContent();
            }

            
            
            return Ok(MapSongsToSongDtos(_manager.GetSongsOfArtist(id)));
        }
        
        [HttpGet("notOfArtist/{id}")]
        public IActionResult GetSongsNotOfArtist(int id)
        {
            if (!_manager.GetSongsNotOfArtist(id).Any())
            {
                return NoContent();
            }
            
            return Ok(MapSongsToSongDtos(_manager.GetSongsNotOfArtist(id)));
        }

        [HttpGet]
        public IActionResult Get()
        {
            if (!_manager.GetAllSongs().Any())
            {
                return NoContent();
            }
            
            return Ok(_manager.GetAllSongs());
        }

        // Song - SongDto mapping

        private IEnumerable<SongDto> MapSongsToSongDtos(IEnumerable<Song> songs)
        {
            IList<SongDto> songDtos = new List<SongDto>();
            foreach(Song song in songs)
            {
                songDtos.Add(MapSongToSongDto(song));
            }

            return songDtos;
        }

        private SongDto MapSongToSongDto(Song song)
        {
            SongDto songDto = new SongDto()
            {
                Id = song.Id,
                Title = song.Title,
                AmountOfStreams = song.AmountOfStreams,
                ReleaseDate = song.ReleaseDate,
                Length = song.Length,
                Genre = song.Genre
            };

            return songDto;
        }
    }
}
﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MusicManager.BL;
using MusicManager.BL.Domain;

namespace UI.MVC.Controllers.Api
{
    
    [Route("api/[controller]")]
    public class RecordLabelsController : ControllerBase
    {
        private readonly IManager _manager;

        public RecordLabelsController(IManager manager)
        {
            _manager = manager;
        }

        [HttpGet]
        public IActionResult Get()
        {
            if (!_manager.GetAllRecordLabels().Any())
            {
                return NoContent();
            }
            
            return Ok(_manager.GetAllRecordLabels());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            if (_manager.GetRecordLabel(id) == null)
            {
                return NoContent();
            }
            
            return Ok(_manager.GetRecordLabel(id));
        }

        
        [HttpPost]
        public IActionResult Post([FromBody] RecordLabel recordLabel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            RecordLabel createdRecordLabel = _manager.AddRecordLabel(recordLabel.Name, recordLabel.DateFounded, recordLabel.Location,null);

            return CreatedAtAction("Get", new {id = createdRecordLabel.Id});
        }
        
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] RecordLabel updatedRecordLabel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            _manager.ChangeRecordLabel(id,updatedRecordLabel);
            return NoContent();
        }
        
    }
}
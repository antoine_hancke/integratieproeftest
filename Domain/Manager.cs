﻿namespace MusicManager.BL.Domain
{
    public class Manager
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public Artist Artist { get; set; }
        
        public Manager(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
    }
}
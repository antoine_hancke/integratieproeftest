﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MusicManager.BL.Domain
{
    public class Song : IValidatableObject
    {
        public int Id { get; set; }
        
        [Required]
        [MinLength(1,ErrorMessage = "The title of a song must be at least 1 character long."),
         MaxLength(100,ErrorMessage = "The title of a song has a maximum length of 100 characters.")]
        public string Title { get; set; }
        
        [Range(0,Int32.MaxValue,ErrorMessage = "The amount of streams can not be negative!")]
        public long? AmountOfStreams { get; set; } // If the song is not yet released, this could be Null.
        public DateTime ReleaseDate { get; set; }
        public TimeSpan Length { get; set; }
        
        public IList<SongContribution> SongContributions { get; set; }
        
        [Required]
        [Range(1,9,ErrorMessage = "The genre given was not valid!")]
        public Genre Genre { get; set; }
        
        public Song() { }

        public Song(string title, long? amountOfStreams, DateTime releaseDate, TimeSpan length, IList<SongContribution> songContributions, Genre genre)
        {
            Title = title;
            AmountOfStreams = amountOfStreams;
            ReleaseDate = releaseDate;
            Length = length;
            SongContributions = songContributions ?? new List<SongContribution>();
            Genre = genre;
        }

        
        // Custom validation for the length of the song (max. 20 minutes long)
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> validationResults = new List<ValidationResult>();

            if (this.Length > new TimeSpan(0, 0, 20, 0))
            {
                string errorMessage = "A song has a maximum length of 20 minutes.";
                validationResults.Add(new ValidationResult(errorMessage,new string[] {"Length"}));
            }

            return validationResults;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MusicManager.BL.Domain
{
    public class RecordLabel
    {
        public int Id { get; set; }
        
        [Required]
        [MinLength(1,ErrorMessage = "The name of a Record Label must be at least 1 character long."),
         MaxLength(50,ErrorMessage = "The name of a Record Label has a maximum length of 50 characters.")]
        public string Name { get; set; }
        public DateTime DateFounded { get; set; }
        public string Location { get; set; }
        public IList<Artist> Artists { get; set; }

        public RecordLabel() { }

        public RecordLabel(string name, DateTime dateFounded, string location, List<Artist> artists)
        {
            Name = name;
            DateFounded = dateFounded;
            Location = location;
            Artists = artists ?? new List<Artist>();
        }
    }
}
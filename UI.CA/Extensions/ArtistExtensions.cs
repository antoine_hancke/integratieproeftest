﻿using MusicManager.BL.Domain;

namespace MusicManager.UI.CA.Extensions
{
    public static class ArtistExtensions
    {
        public static string GetInfo(this Artist artist)
        {
            return $"{artist.Alias} formally known as {(artist.Name.Length == 0 ? "Unknown" : artist.Name)} - " +
                   $"({(artist.SignedRecordLabel == null ? "No Label": artist.SignedRecordLabel.Name)}) " +
                   $"(managed by {(artist.Manager == null ? "No Manager": artist.Manager.GetInfo())})";
        }
    }
}
﻿using MusicManager.BL.Domain;

namespace MusicManager.UI.CA.Extensions
{
    public static class ManagerExtensions
    {
        public static string GetInfo(this Manager manager)
        {
            return $"{manager.FirstName} {manager.LastName}";
        }
    }
}
﻿using System;
using MusicManager.BL.Domain;

namespace MusicManager.UI.CA.Extensions
{
    public static class RecordLabelExtensions
    {
        public static string GetInfo(this RecordLabel recordLabel)
        {
            return $"{recordLabel.Name} (founded on {(recordLabel.DateFounded == DateTime.MinValue ? "Unknown" : recordLabel.DateFounded.ToString("dd/MM/yyyy"))}" +
                   $" in {(recordLabel.Location.Length == 0 ? "Unknown" : recordLabel.Location)})";
        }
    }
}
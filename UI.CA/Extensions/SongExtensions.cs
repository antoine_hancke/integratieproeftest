﻿using System.Text;
using MusicManager.BL.Domain;

namespace MusicManager.UI.CA.Extensions
{
    public static class SongExtensions
    {
        public static string GetInfo(this Song song)
        {
            StringBuilder returnString = new StringBuilder($"{song.Title} ({song.Genre})");

            if (song.SongContributions != null)
            {
                if (song.SongContributions.Count > 0)
                {
                    returnString.Append(" - ");
                
                    for(int i = 0; i < song.SongContributions.Count-1;i++)
                    {
                        returnString.Append(song.SongContributions[i].Artist.Alias + ", ");
                    }
                    
                    returnString.Append(song.SongContributions[song.SongContributions.Count-1].Artist.Alias);
                }
            }

            return returnString.ToString();
        }
    }
}